//
// Created by Tony York on 30.10.2022.
//

#ifndef LAB_1_BMP_H
#define LAB_1_BMP_H

#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //LAB_1_BMP_H
