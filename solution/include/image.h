//
// Created by Tony York on 31.10.2022.
//

#ifndef LAB_1_IMAGE_H
#define LAB_1_IMAGE_H

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //LAB_1_IMAGE_H
