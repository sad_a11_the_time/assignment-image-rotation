//
// Created by Tony York on 31.10.2022.
//

#ifndef LAB_1_ROTATE_H
#define LAB_1_ROTATE_H

#include "../include/image.h"
#include "utils.h"

struct image rotate( const struct image* img );

#endif //LAB_1_ROTATE_H
