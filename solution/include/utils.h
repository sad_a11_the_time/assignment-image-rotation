//
// Created by Tony York on 02.11.2022.
//

#ifndef LAB_1_UTILS_H
#define LAB_1_UTILS_H

#include "image.h"
#include <stdint.h>
#include <stdlib.h>

void my_copy(void* dst, const void* src, size_t size);

#endif //LAB_1_UTILS_H
