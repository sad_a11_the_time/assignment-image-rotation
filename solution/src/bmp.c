//
// Created by Tony York on 31.10.2022.
//

#include "../include/bmp.h"
#include "../include/utils.h"

#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint32_t calculate_padding(uint32_t width) {
    return (4 - ((width * 3) % 4));
}

struct bmp_header generate_bmp_header(const struct image* img){
    return (struct bmp_header) {.bfType = 0x4D42, .bOffBits = 54, .biWidth = img->width, .biHeight = img->height, .bfileSize = (img->width * 3 + calculate_padding(img->width)) * img->height + 54, .biSize = 40, .biPlanes = 1, .biBitCount = sizeof(struct pixel) * 8, .biCompression = 0, .biSizeImage = 0, .biXPelsPerMeter = 0, .biYPelsPerMeter = 0, .biClrUsed = 0, .biClrImportant = 0};
}

enum read_status from_bmp(FILE* in, struct image* img) {

    struct bmp_header header;
    size_t size1 = fread(&header, 1, sizeof(struct bmp_header), in);

    if (size1 != sizeof(struct bmp_header)) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    img->width = header.biWidth;
    img->height = header.biHeight;

    uint32_t off = header.bOffBits;
    fseek(in, off, 0);

    uint64_t pictureSize = (calculate_padding(img->width) + img->width * 3) * img->height;

    char* pic = malloc(pictureSize);
    size_t size2 = fread(pic, 1, pictureSize, in);
    if (size2 != pictureSize) {
        free(pic);
        return READ_ERROR;
    }

    img->data = malloc(3 * img->width * img->height);
    uint64_t p = 0;
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            my_copy(img->data + (i * img->width + j), pic + p, sizeof(struct pixel));
            p += 3;
        }
        p += calculate_padding(img->width);
    }

    free(pic);
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {

    struct bmp_header header = generate_bmp_header(img);
    if (out == NULL) {
        return WRITE_ERROR;
    }
    size_t size1 = fwrite(&header, 1, sizeof(struct bmp_header), out);
    if (size1 != sizeof(struct bmp_header)) {
        return WRITE_ERROR;
    }

    uint64_t pictureSize = (calculate_padding(img->width) + img->width * 3) * img->height;

    char* pic = calloc(pictureSize, 1);
    uint64_t p = 0;
    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            my_copy(pic + p, img->data + (i * img->width + j), sizeof(struct pixel));
            p += 3;
        }
        p += calculate_padding(img->width);
    }


    size_t size2 = fwrite(pic, 1, pictureSize, out);
    if (size2 != pictureSize) {
        free(pic);
        return WRITE_ERROR;
    }

    free(pic);
    return WRITE_OK;
}
