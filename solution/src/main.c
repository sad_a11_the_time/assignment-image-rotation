#include "../include/rotate.h"
#include "../include/bmp.h"

#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {

    (void) argc; (void) argv;

    FILE* in = NULL;
    FILE* out = NULL;
    struct image image;

    char *input_file = argv[1];
    char *output_file = argv[2];

    in = fopen( input_file, "rb" );
    if( !in ) {
        fprintf( stderr, "something went wrong; the file cannot be opened");
        exit(1);
    }

    out = fopen( output_file, "wb" );
    if( !out ) {
        fprintf( stderr, "something went wrong; the file cannot be opened");
        exit(1);
    }

    enum read_status rs = from_bmp( in, &image);
    if( rs != READ_OK ) {
        fprintf( stderr, "something went wrong; the file cannot be read");
    }

    struct image img_rot = rotate( &image );

    enum write_status ws = to_bmp( out, &img_rot );
    if( ws != WRITE_OK ) {
        fprintf( stderr, "something went wrong; the file cannot be edited");
    }

    fclose( in );
    fclose( out );

    free(image.data);
    free(img_rot.data);

    return 0;
}
