//
// Created by Tony York on 01.11.2022.
//

#include "../include/rotate.h"

#include <stdlib.h>

struct image rotate( const struct image* img ) {

    struct image transformed_img = {
        .height = img -> width,
        .width = img -> height
    };

    transformed_img.data = malloc(sizeof(struct pixel) * img -> width * img -> height);

    for (uint64_t i = 0; i < transformed_img.height; ++i){
        for (uint64_t j = 0; j < transformed_img.width; ++j){
            transformed_img.data[i * transformed_img.width + transformed_img.width - j - 1] = img->data[j * img->width + i];
        }
    }

    return transformed_img;
}
