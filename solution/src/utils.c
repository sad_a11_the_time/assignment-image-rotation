//
// Created by Tony York on 02.11.2022.
//

#include "../include/utils.h"

void my_copy(void* dst, const void* src, size_t size) {
    char* char_dst = (char*)dst;
    const char* char_src = (const char*)src;

    for (size_t i = 0; i < size; ++i) {
        char_dst[i] = char_src[i];
    }
}
